// BASIC FRAMEWORKS
// ---------------------

// MODELS
// ---------------------
var model = require('../model');

// CONTROLLERS
// ---------------------
var util = require("./");

/**
 * @api {get} /status status
 * @apiVersion 1.0.0
 * @apiName status
 * @apiGroup Health
 * @apiPermission None
 *
 * @apiDescription this method returns the status of the API, healthcheck can determine if node is running
 */
exports.injectApiStatusRoute = function injectApiStatusRoute(Router) {
    Router.route("/status").get(function (req, res) {
        __isApiStatusHealthy(function (isHealthy) {
            if (isHealthy) {
                util.Response.sendOK(req, res); // OK
            } else {
                util.Response.sendError(req, res); // FAIL
            }
        });
    });
};

/**
 * check if the api status is healthy
 * checks therefore the database connections (mysql, mongo)
 * @param callback function(isHealthy)
 * @private
 */
function __isApiStatusHealthy(callback) {
    // check mongo connection
    model.mongo.isConnected(function (isConnected) {
        if (!isConnected) return callback(false); // failed

        callback(true); // success
    });
}