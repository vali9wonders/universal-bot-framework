// BASIC FRAMEWORKS
// ---------------------
var http = require('http');

// MODELS
// ---------------------
var model = require('../model');

// CONTROLLERS
// ---------------------
var util = require("./");

// GEOCODER
// https://github.com/nchaulet/node-geocoder
// ---------------------
var geocoderProvider = 'google';
var httpAdapter = 'https';
// optional
var extra = {
    apiKey: global.bots.config.google.geocoderApiKey
    //formatter: null // 'gpx', 'string', ...
};

var geocoder = require('node-geocoder')(geocoderProvider, httpAdapter, extra);

var allVotesOrSkips = [];

/**
 * gets the IP of a
 * @param ip string
 * @param callback callback(location), location is null if failed
 *                          location.latitude;
 *                          location.longitude;
 */
exports.getLocationOfIP = function getLocationOfIP(ip, callback) {
    // console.log("getLocationOfIP: " + ip);
    if ((!ip || ip == "null") && callback) { // IP is empty
        return callback(null);
    }
    // get location from freegeoip.net, max. 10.000/h
    var options = {
        host: 'freegeoip.net',
        port: 8080,
        path: '/json/' + ip,
        method: 'GET'
    };

    var req = http.request(options, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            if (chunk.indexOf("{") === -1) {
                console.error("cant get location of IP: '" + ip + "' | error: " + chunk);
                if (callback) {
                    callback(null);
                }
                return;
            }
            var location = JSON.parse(chunk);
            // console.log("got IP location");
            if (callback) {
                callback(location);
            }
        });
        req.on('error', function (e) {
            console.error('problem with request: ' + e.message);
        });
    });

    req.end();
};

/**
 * returns the address of a location
 * @param lat
 * @param long
 * @param callback
 */
exports.getAddressOfLocation = function getAddressOfLocation(lat, long, callback) {
    // Using callback
    geocoder.reverse({lat: lat, lon: long}, function (error, result) {
        if (error || result.length == 0) {
            console.error("getAddressOfLocation: ", error);
            callback(null);
            return;
        }
        if (result.length > 0) {
            callback(result[0]);
        }
    });
};

/**
 * returns the location of a address or search term
 * @param searchTerm
 * @param callback
 */
exports.getLocationOfAddress = function getLocationOfAddress(searchTerm, callback) {
    // Using callback
    geocoder.geocode(searchTerm, function (error, result) {
        if (error || result.length == 0) {
            console.error("getLocationOfAddress: ", error);
            callback(null);
            return;
        }
        if (result.length > 0) {
            callback({
                lat: result[0].latitude,
                long: result[0].longitude
            });
        }
    });
};