module.exports.Handler = require("./Handler");
module.exports.Kik = require("./kik/index");
module.exports.Messenger = require("./messenger/index");
module.exports.Telegram = require("./telegram/index");
module.exports.Skype = require("./skype/index");