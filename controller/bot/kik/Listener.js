// BASIC FRAMEWORKS
// ---------------------
var Bot = require('@kikinteractive/kik');

// MODELS
// ---------------------
var model = require('../../../model');

// CONTROLLERS
// ---------------------
var util = require("../../../util");
var controller = require("../../../controller");

// CONST
// ---------------------
var constant = require("../../../constant");
var _chatBotPlatform = "Kik"; // case-sensitive, must be the same as in config.bot.json and bot/index.js

// CONFIG
// ---------------------
var config = require("../../../config/config.bot")[global.bots.stageToUse][_chatBotPlatform];

exports.isEnabled = function isEnabled() {
    return !util.Helpers.isEmpty(config.username);
};
if (!exports.isEnabled()) {
    console.warn("Not initializing Kik bot");
    return;
}

// Initialize Bot
var bot = new Bot({
    username: config.username,
    apiKey: config.apiKey,
    baseUrl: config.baseUrl,
    incomingPath: config.incomingPath
});
bot.updateBotConfiguration(); // sends a call to Kik backend to update settings

bot.onStartChattingMessage(function (incoming) {
    console.log("onStartChattingMessage", incoming);
    __processMessage(incoming.from, {action: constant.BOT.MESSAGE.START}, {});
});

bot.onScanDataMessage(function (incoming) {
    console.log("onScanDataMessage", incoming);
    __processMessage(incoming.from, {action: constant.BOT.MESSAGE.START}, {});
});

bot.onTextMessage(function (incoming) {
    console.log("onTextMessage incoming", incoming.from, incoming.body);
    __processMessage(incoming.from, {action: incoming.body, message: true}, {});
});

bot.onLinkMessage(function (incoming) {
    console.log("onLinkMessage", incoming);
    __processMessage(incoming.from, {action: constant.BOT.MESSAGE.START}, {});
});

bot.onPictureMessage(function (incoming) {
    console.log("onPictureMessage", incoming);
    __processMessage(incoming.from, {action: constant.BOT.MESSAGE.START}, {});
});

bot.onVideoMessage(function (incoming) {
    console.log("onVideoMessage", incoming);
    __processMessage(incoming.from, {action: constant.BOT.MESSAGE.START}, {});
});

bot.onStickerMessage(function (incoming) {
    console.log("onStickerMessage", incoming);
    __processMessage(incoming.from, {action: constant.BOT.MESSAGE.START}, {});
});

//bot.getKikCodeUrl()
//    .then(function (code) {
//        console.log("code", code);
//    });

/**
 * this is the first method after the webhook is called
 * it starts all the rest, session, processing, ...
 * @param chatBotUserId
 * @param payload object {action: ""}
 * @param req
 * @private
 */
function __processMessage(chatBotUserId, payload, req) {
    console.log("__processMessage", chatBotUserId, payload);
    controller.Bot.Handler.processMessage(
        _chatBotPlatform, // chatBotPlatform
        chatBotUserId, // chatBotUserId
        payload, // payload
        req, // req
        function (error, response) {
            console.log("processMessage:after", error, response);
        }
    );
}

bot.isEnabled = exports.isEnabled;
module.exports = bot;