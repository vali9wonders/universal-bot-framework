// BASIC FRAMEWORKS
// ---------------------
var TeleBot = require('./telebot');

// MODELS
// ---------------------
var model = require('../../../model');

// CONTROLLERS
// ---------------------
var util = require("../../../util");
var controller = require("../../../controller");

// CONST
// ---------------------
var constant = require("../../../constant");
var _chatBotPlatform = "Telegram"; // case-sensitive, must be the same as in config.bot.json and bot/index.js

// CONFIG
// ---------------------
var config = require("../../../config/config.bot")[global.bots.stageToUse][_chatBotPlatform];

exports.isEnabled = function isEnabled() {
    return !util.Helpers.isEmpty(config.accessToken);
};
if (!exports.isEnabled()) {
    console.warn("Not initializing Telegram bot");
    return;
}

// Initialize Bot
var bot = new TeleBot({
    token: config.accessToken,
    timeout: 1,
    //webhook: {
    //    url: "https://api-development.letsdvel.com/v1/bot/telegram",
    //    host: "0.0.0.0",
    //    port: 8443
    //}
});

// On every text message
bot.on(['text', 'edited'], function (incoming, self) {
    console.log("text", incoming);
    var text = incoming.text.replace("/", ""); // replace the trailing slash from commands
    __processMessage(incoming.from.id, {action: text}, {});
});

bot.on('photo', function (incoming) {
    console.log("text", incoming);
    __processMessage(incoming.from.id, {action: constant.BOT.MESSAGE.START}, {});
});

// On location message
bot.on('location', function (incoming) {
    console.log("location", incoming.location);
    // incoming.location: { latitude: 34.014034, longitude: -118.494241 }
    __processMessage(incoming.from.id, {
        action: constant.BOT.MESSAGE.LOCATION_SENT, location: {
            lat: incoming.location.latitude,
            long: incoming.location.longitude
        }
    }, {});
});

// On contact message
bot.on('contact', function (incoming) {
    console.log("contact", incoming);
    __processMessage(incoming.from.id, {action: constant.BOT.MESSAGE.START}, {});
});

// On other unsupported events
bot.on([
    'audio',
    'voice',
    'document',
    'sticker',
    'video',
    'venue'
], function (incoming) {
    console.log("unsupported event", incoming);
    __processMessage(incoming.from.id, {action: constant.BOT.MESSAGE.START}, {});
});

// Inline button callback
bot.on('callbackQuery', function (incoming) {
    //console.log("callbackQuery", incoming);

    __processMessage(incoming.from.id, JSON.parse(incoming.data), {});

    // User message alert
    //bot.answerCallback(incoming.id, constant.BOT.MESSAGE.THANKS_FOR_VOTING, false);

    // remove buttons after voting
    controller.Bot.Telegram.Controller.editMarkup(incoming.message.chat.id, incoming.message.message_id);
});

// Inline query
//bot.on('inlineQuery', function (msg) {
//    console.log("inline Query", msg);
//
//    var query = msg.query;
//    var answers = bot.answerList(msg.id);
//
//    answers.addArticle({
//        id: 'query',
//        title: 'Inline Query',
//        description: `Your query: ${ query }`,
//        message_text: 'Click!'
//    });
//    bot.answerQuery(answers);
//});

bot.connect();

/**
 * this is the first method after the webhook is called
 * it starts all the rest, session, processing, ...
 * @param chatBotUserId
 * @param payload object {action: ""}
 * @param req
 * @private
 */
function __processMessage(chatBotUserId, payload, req) {
    console.log("__processMessage", chatBotUserId, payload);
    controller.Bot.Handler.processMessage(
        _chatBotPlatform, // chatBotPlatform
        chatBotUserId, // chatBotUserId
        payload, // payload
        req, // req
        function (error, response) {
            console.log("processMessage:after", error, response);
        }
    );
}

bot.isEnabled = exports.isEnabled;
module.exports = bot;