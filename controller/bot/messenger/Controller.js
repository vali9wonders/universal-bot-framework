// BASIC FRAMEWORKS
// ---------------------

// MODELS
// ---------------------
var model = require('../../../model');

// CONTROLLERS
// ---------------------
var util = require("../../../util");
var controller = require("../../../controller");

// CONST
// ---------------------
var constant = require("../../../constant");
var _chatBotPlatform = "Messenger"; // case-sensitive, must be the same as in config.bot.json and bot/index.js
var _bot = require("./Listener"),
    _maxButtons = 3,
    _maxElements = 10,
    _maxQuickReplies = 10;

/**
 * sends a welcome message to the user
 * @param chatBotUserSession
 */
exports.sendWelcomeMessage = function sendWelcomeMessage(chatBotUserSession) {
    var text = constant.BOT.MESSAGE.WELCOME.replace(":first_name:", chatBotUserSession.profile.first_name);
    exports.sendText(chatBotUserSession, text);
    exports.sendCommandChooser(chatBotUserSession);
};

/**
 * send command chooser
 * @param chatBotUserSession
 */
exports.sendCommandChooser = function sendCommandChooser(chatBotUserSession) {
    var text = constant.BOT.MESSAGE.WHAT_WOULD_YOU_LIKE_TO_DO;
    // max 3 buttons
    var buttons = [
        {
            "type": "postback",
            "title": constant.BOT.MESSAGE.START_VOTING,
            "payload": JSON.stringify({action: constant.BOT.MESSAGE.START_VOTING})
        },
        {
            "type": "postback",
            "title": constant.BOT.MESSAGE.CHOOSE_A_CATEGORY,
            "payload": JSON.stringify({action: constant.BOT.MESSAGE.CHOOSE_A_CATEGORY})
        },
        {
            "type": "web_url",
            "title": constant.BOT.MESSAGE.SEND_LINK,
            "url": constant.BOT.MESSAGE.WEB_URL
        }
    ];
    _bot.sendButtonMessage(chatBotUserSession.chatBotUserId, text, buttons, function (error) {
        if (error) {
            console.error("sendCommandChooser:sendButtonMessage", error);
        }
    });
};

/**
 * send category chooser
 * @param chatBotUserSession
 * @param categories
 */
exports.sendCategoryChooserWithScrollView = function sendCategoryChooserWithScrollView(chatBotUserSession, categories) {
    var elements = [],
        counter = 0;
    for (var i in categories) {
        if (categories.hasOwnProperty(i) && counter < _maxElements) {
            elements.push(
                {
                    "title": categories[i].name,
                    "image_url": categories[i].icons.large,
                    //"subtitle": "any text",
                    "buttons": [
                        {
                            "type": "postback",
                            "title": constant.BOT.MESSAGE.SELECT_CATEGORY_PREFIX + " " + categories[i].name,
                            "payload": JSON.stringify({
                                action: constant.BOT.MESSAGE.CHANGE_CATEGORY_ACTION,
                                categorySlug: categories[i].slug
                            })
                        }
                    ]
                });
        }
        counter++;
    }

    _bot.sendGenericMessage(chatBotUserSession.chatBotUserId, elements, function (error) {
        if (error) {
            console.error("sendCategoryChooserWithScrollView: callback sendGenericMessage sent", error);
        }
        exports.sendTrendingTags(chatBotUserSession);
    });
};

/**
 * send dvel to vote
 * @param chatBotUserSession
 * @param dvel
 */
exports.sendDvelToVote = function sendDvelToVote(chatBotUserSession, dvel) {

    var picUrl = (constant.BOT.MESSAGE.SWELL_SHARING_IMAGE_URL_PREFIX + (global.bots.config.production ? dvel.shortId : "ryXP28Gz")) + ".png";

    _bot.sendImageMessage(chatBotUserSession.chatBotUserId, picUrl, function (error, result) {
        if (error) {
            console.error("callback sendImageMessage sent", error, result);
        }
        // send caption + bubbles
        _bot.sendQuickReplyMessageWithAttachment(
            chatBotUserSession.chatBotUserId,
            dvel.user.name + ": " + (dvel.question || "#whatsBetter"),
            null, // picUrl
            [
                {
                    "type": "web_url",
                    "url": "http://swell.wtf/d/" + dvel.shortId,
                    "title": constant.BOT.MESSAGE.OPEN_IN_APP_EMOJI
                }
            ],
            [
                {
                    "content_type": "text",
                    "title": constant.BOT.MESSAGE.VOTE_A,
                    "payload": JSON.stringify({
                        action: constant.BOT.MESSAGE.DO_VOTE_ACTION,
                        dvelId: dvel.id,
                        photoId: dvel.photos[0].id
                    })
                },
                {
                    "content_type": "text",
                    "title": constant.BOT.MESSAGE.SKIP_EMOJI,
                    "payload": JSON.stringify({action: constant.BOT.MESSAGE.DO_SKIP_ACTION, dvelId: dvel.id})
                },
                {
                    "content_type": "text",
                    "title": constant.BOT.MESSAGE.VOTE_B,
                    "payload": JSON.stringify({
                        action: constant.BOT.MESSAGE.DO_VOTE_ACTION,
                        dvelId: dvel.id,
                        photoId: dvel.photos[1].id
                    })
                }
            ],
            function (error, result) {
                if (error) {
                    console.error("sendDvelToVote: callback quickReply sent", error, result);
                }
            }
        );
    });
};

/**
 * this method gets called from Bot.Controller
 * it returns a generic user profile with the same keys for every chat bot platform
 * @param chatBotUserId
 * @param callback function(profile), profile could be null if not found
 */
exports.getUserProfileGeneric = function getUserProfileGeneric(chatBotUserId, callback) {
    console.log("getUserProfileGeneric", chatBotUserId);
    _bot.getUserProfile(chatBotUserId, function (error, user) {
        //console.log("getUserProfileGeneric:getUserProfile", user);
        if (user)
            callback({
                id: chatBotUserId,
                first_name: user.first_name,
                last_name: user.last_name,
                profile_pic: user.profile_pic,
                locale: user.locale,
                timezone: user.timezone,
                gender: user.gender ? user.gender == "male" ? 0 : 1 : 99
            });
        else {
            callback(null);
        }
    })
};

/**
 * send text
 * @param chatBotUserSession
 * @param text
 */
exports.sendText = function sendText(chatBotUserSession, text) {
    _bot.sendTextMessage(chatBotUserSession.chatBotUserId, text);
};

/**
 * send link
 * @param chatBotUserSession
 * @param link
 * @param text
 * @param picUrl
 */
exports.sendLink = function sendLink(chatBotUserSession, link, text, picUrl) {
    var buttons = [
        {
            "type": "web_url",
            "title": text,
            "url": link
        }
    ];
    _bot.sendButtonMessage(chatBotUserSession.chatBotUserId, text, buttons);
};

/**
 * remove voting buttons
 * @param chatBotUserSession
 */
exports.removeButtons = function removeButtons(chatBotUserSession) {
    if (chatBotUserSession.settings.lastMessage && chatBotUserSession.settings.lastMessage.chatId && chatBotUserSession.settings.lastMessage.messageId) {

        chatBotUserSession.settings.lastMessage = null;
        chatBotUserSession.save(function (error, result) {
            if (error) console.error("removeButtons.settings.lastMessage save", error);
        });
    }
};

/**
 * ask for users location
 * @param chatBotUserSession
 */
exports.askForLocation = function askForLocation(chatBotUserSession) {
    chatBotUserSession.settings.specialAction = constant.BOT.SPECIAL_ACTION.ASK_FOR_LOCATION;
    chatBotUserSession.save(function (error, result) {
        if (error) console.error("askForLocation:chatBotUserSession.settings.specialAction save", error);
    });

    var text = constant.BOT.MESSAGE.ASK_FOR_LOCATION;
    var buttons = [
        {
            "type": "postback",
            "title": constant.BOT.MESSAGE.NO_LATER,
            "payload": JSON.stringify({action: constant.BOT.MESSAGE.NO_LATER})
        }
    ];
    _bot.sendButtonMessage(chatBotUserSession.chatBotUserId, text, buttons);

    // TODO maybe send a link to a webpage which then calls the endpoint and the endpoint does a IP to location and adds it to the userSession

};

/**
 * gets called if location not found through address search
 * @param chatBotUserSession
 */
exports.locationNotFoundAskAgain = function locationNotFoundAskAgain(chatBotUserSession) {
    var text = constant.BOT.MESSAGE.SRY_CANT_FIND_CITY;
    var buttons = [
        {
            "type": "postback",
            "title": constant.BOT.MESSAGE.CANCEL,
            "payload": JSON.stringify({action: constant.BOT.MESSAGE.CANCEL})
        }
    ];
    _bot.sendButtonMessage(chatBotUserSession.chatBotUserId, text, buttons);
};

/**
 * sends trending tags to user
 * @param chatBotUserSession
 */
exports.sendTrendingTags = function sendTrendingTags(chatBotUserSession) {
    var trendingTags = controller.Bot.Handler.getTrendingTags(_maxQuickReplies);
    var quickReplies = [];
    trendingTags.forEach(function (tag) {
        quickReplies.push({
            "content_type": "text",
            "title": tag,
            "payload": JSON.stringify({action: tag})
        });
    });

    _bot.sendQuickReplyMessage(
        chatBotUserSession.chatBotUserId,
        constant.BOT.MESSAGE.TRENDING_TAGS + ":",
        quickReplies,
        function (error, result) {
            if (error) {
                console.error("sendTrendingTags: callback quickReply sent", error, result);
            }
        }
    );
};