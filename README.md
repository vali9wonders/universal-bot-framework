# Universal Bot Framework

This is a boilerplate to build a Universal Chatbots API (crossplatform). 

You can build an 'crossplatform' API for **FB Messenger, Telegram, Kik and Skype** with just implementing **one** logic! =)

![universal-bot-framework.png](https://bitbucket.org/repo/M87GoB/images/1715112437-universal-bot-framework.png)

Check it out. If you have questions, ask me ;)

[Installation >](https://bitbucket.org/phips28/universal-bot-framework/wiki/Installation)

[Usage >](https://bitbucket.org/phips28/universal-bot-framework/wiki/Usage)


-----

## Contribute

All contributions are welcome.

## License

Copyright (c) 2016 Philipp Holly - Released under The MIT License.